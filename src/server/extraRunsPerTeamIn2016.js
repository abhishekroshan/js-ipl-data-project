const csv = require("csv-parser");
const fs = require("fs");
//importing the fs and csv-parser

const allMatches = [];
const deliveries = [];
//creating an array to store all the matches from the csv file

function extraRunsPerTeamIn2016(allMatches, deliveries) {
  const extraRunsConcededPerTeam = {};
  //the resultant object
  for (let index = 0; index < allMatches.length; index++) {
    if (allMatches[index].season === "2016") {
      const matchId = allMatches[index].id;
      /*
      Iterating through the allMatches array and if the season is 
      2016 we store its id in the matchId
      */
      for (let newIndex = 0; newIndex < deliveries.length; newIndex++) {
        if (deliveries[newIndex].match_id === matchId) {
          /*------------------------------------------------------- */
          const bowlingTeam = deliveries[newIndex].bowling_team;
          const extras = Number(deliveries[newIndex].extra_runs);
          /*Iterating through the deliveries array and if the id of current index
          of the deliveries array matches the required id 
          we store the value of bowling team and extra runs in new variable */

          if (!extraRunsConcededPerTeam[bowlingTeam]) {
            extraRunsConcededPerTeam[bowlingTeam] = 0;
          }
          //if the extraRunswith the bowling team is not created we create it
          //as a key and assign its value as 0;
          extraRunsConcededPerTeam[bowlingTeam] += extras;

          //we increment the value with the number of extraRuns
        }
      }
    }
  }
  return extraRunsConcededPerTeam;
  //return the object
}

fs.createReadStream("../data/matches.csv")
  //reading the .csv file
  .pipe(csv())
  .on("data", (data) => allMatches.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      //reading the .csv file
      .pipe(csv())
      .on("data", (data) => deliveries.push(data))
      //pushing the values in the deliveries Array
      .on("end", () => {
        try {
          const extraRuns = extraRunsPerTeamIn2016(allMatches, deliveries);
          //calling the function extraRunsPerTeamIn2016 with allMatch and deliveries array.

          const output = "../public/output/extraRunsPerTeamIn2016.json";
          //the output is the location where the .json file needs to be dumped

          fs.writeFile(output, JSON.stringify(extraRuns, null, 2), (err) => {
            /*Passing the output variable to the writeFile function and
        with the JSON.stringify with the matchCount variable which
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
            if (err) throw err;
          });
        } catch (error) {
          console.log(error);
          //log the error if an error happens
        }
      });
  });
