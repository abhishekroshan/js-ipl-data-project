const csv = require("csv-parser");
const fs = require("fs");
//Importing fs and csv-parser

const allMatches = [];
//storing all the matches in the array

function tossAndMatchWinner(array) {
  const matchAndTossWinner = {};
  //creating an object to store all the values

  for (let index = 0; index < array.length; index++) {
    const tossWinner = array[index].toss_winner;
    const matchWinner = array[index].winner;
    //storing the value of the currentIndex tosswinner and matchWinner

    if (tossWinner === matchWinner) {
      if (matchAndTossWinner[tossWinner]) {
        matchAndTossWinner[tossWinner]++;
        //if tossWinner and MatchWinner is same we check is the key is present or not
        //if present we increment its value
      } else {
        matchAndTossWinner[tossWinner] = 1;
        //if the key is not present we make a newKey with the name of tossWinner
        //and set its value to 1;
      }
    }
  }
  return matchAndTossWinner;
  //return the object with result
}

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  //reading the .csv file
  .on("data", (data) => allMatches.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    try {
      const count = tossAndMatchWinner(allMatches);
      //calling the function with the allMatches array and storing in Count variabl
      const output = "../public/output/teamWinningTossAndMatch.json";
      //the output is the location where the .json file needs to be dumped
      fs.writeFile(output, JSON.stringify(count, null, 2), (err) => {
        /*Passing the output variable to the writeFile function and 
        with the JSON.stringify with the Count variable which 
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
        if (err) throw err;
      });
    } catch (error) {
      console.log(error);
      //log the error if an error happens
    }
  });
