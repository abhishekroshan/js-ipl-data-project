const csv = require("csv-parser");
const fs = require("fs");
//Importing fs and csv-parser

const allMatches = [];
//storing all the matches in the array

function playerOfTheMatch(array) {
  let allPlayerOfMatch = {};
  //creating an object to store the values

  for (let index = 0; index < array.length; index++) {
    const season = array[index].season;
    const playerOfTheMatch = array[index].player_of_match;
    //storing the playerof the match and current season in the variable

    if (playerOfTheMatch) {
      if (!allPlayerOfMatch[season]) {
        allPlayerOfMatch[season] = {};
        //if the current season is not present we add the current season
        //into the object as a new empty object.
      }

      if (allPlayerOfMatch[season][playerOfTheMatch]) {
        allPlayerOfMatch[season][playerOfTheMatch]++;
        //if the player exists in the object and has playerOfMatch award
        //we increment its count;
      } else {
        allPlayerOfMatch[season][playerOfTheMatch] = 1;
        //if the player of the match doesnt exists in the object we initialize its count as 0.
      }
    }
  }

  let playerWithMostAward = {};
  //new object to store most player of the award per season;

  for (let key in allPlayerOfMatch) {
    //iterating through the array which contains allPlayer of the match award.
    playerWithMostAward[key] = {};
    //creating the object with the current value
    let players = allPlayerOfMatch[key];
    //currentPlayer in the iteration
    let mostPlayerWithAward = "";
    //empty string
    let mostNumberOfAward = 0;
    //mostNumberOfAward currently initialized to zero, it will
    //be helpful to compare value.

    for (let newKey in players) {
      //iterating in the current year in the object
      if (players[newKey] > mostNumberOfAward) {
        mostNumberOfAward = players[newKey];
        mostPlayerWithAward = newKey;
        //if the value of the current item is greater..
        //we assign change the value of mostPlayerWith award and most Award
        //with that perticluar item
      }
    }
    playerWithMostAward[key][mostPlayerWithAward] = mostNumberOfAward;
    //now we create an new key value pair inside the new object
  }
  return playerWithMostAward;
}

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  //reading the .csv file
  .on("data", (data) => allMatches.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    try {
      const result = playerOfTheMatch(allMatches);
      //calling the function with the allMatches array and storing in Count variable
      const output = "../public/output/playerOfMatch.json";
      //the output is the location where the .json file needs to be dumped
      fs.writeFile(output, JSON.stringify(result, null, 2), (err) => {
        /*Passing the output variable to the writeFile function and 
        with the JSON.stringify with the result variable which 
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
        if (err) throw err;
      });
    } catch (error) {
      console.log(error);
      //log the error if an error happens
    }
  });
