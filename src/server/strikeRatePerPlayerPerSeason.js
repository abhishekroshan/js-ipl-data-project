const csv = require("csv-parser");
const fs = require("fs");

const allMatches = [];
const deliveries = [];
//storing the value from the csv file in the allMatches and deliveries array

function strikeRate(allMatches, deliveries) {
  const result = {};
  //this result object will carry the runs and balls played by every batsman in every year

  for (let index = 0; index < allMatches.length; index++) {
    const year = allMatches[index].season;
    const id = allMatches[index].id;
    //iterating in the allmatches array and stroing the current season
    //and id of that index in variable.

    if (!result[year]) {
      result[year] = {};
    }
    //create a new object inside the object by the name of current season/year

    for (let newIndex = 0; newIndex < deliveries.length; newIndex++) {
      const delivery = deliveries[newIndex];
      //iterating in the deliveries array and storing the properties
      //of that index in the delivery variable.

      if (delivery.match_id === id) {
        const player = delivery.batsman;
        const playerRuns = Number(delivery.batsman_runs);
        /*
        if the match_id corrosponds to the id means they are the same match &
        we store the batsman name and the runs he scored on that index 
        in seperate variable
        */

        if (!result[year][player]) {
          result[year][player] = {
            totalRuns: 0,
            totalBalls: 0,
          };
        }
        //if the player name is not in the object we initialise it
        //as a new object with totalRuns and totalBalls as 0.

        result[year][player].totalRuns += playerRuns;
        result[year][player].totalBalls++;
        //we add the player runs to the value
        //we increment the number of balls by 1.
      }
    }
  }
  const finalResult = {};
  //the main object which contains the desired value.

  for (const index in result) {
    finalResult[index] = {};
    //iterating in the result object and creating an new object inside finalResult object
    for (const newIndex in result[index]) {
      finalResult[index][newIndex] = {};
      //iterating in the result[index] object
      const runs = Number(result[index][newIndex].totalRuns);
      const balls = Number(result[index][newIndex].totalBalls);
      //created variable runs and balls to store the value in integer form

      finalResult[index][newIndex] = ((runs / balls) * 100).toFixed(2);
      //assigned the strike-rate and rounded off it value to 2 decimal points
    }
  }
  return finalResult;
}

fs.createReadStream("../data/matches.csv")
  //reading the .csv file
  .pipe(csv())
  .on("data", (data) => allMatches.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      //reading the .csv file
      .pipe(csv())
      .on("data", (data) => deliveries.push(data))
      //pushing the values in the deliveries Array
      .on("end", () => {
        try {
          const result = strikeRate(allMatches, deliveries);
          //calling the function extraRunsPerTeamIn2016 with allMatch and deliveries array.

          const output = "../public/output/strikeRatePerPlayerPerSeason.json";
          //the output is the location where the .json file needs to be dumped

          fs.writeFile(output, JSON.stringify(result, null, 2), (err) => {
            /*Passing the output variable to the writeFile function and
        with the JSON.stringify with the matchCount variable which
        contains the desired v
        alue.
        with a callback with error if the error happens it will execute the catch block */
            if (err) throw err;
          });
        } catch (error) {
          console.log(error);
          //log the error if an error happens
        }
      });
  });
