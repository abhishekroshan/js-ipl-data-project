const csv = require("csv-parser");
const fs = require("fs");
//Importing fs and csv-parser

const allMatches = [];
//storing all the matches in the array

function matchesWonPerTeamPerYear(array) {
  let winningMatches = {};
  //creating an object to store the values

  for (let index = 0; index < array.length; index++) {
    const year = array[index].season;
    const winner = array[index].winner;
    //storing the season and the winner in the year and winner variable

    if (!winningMatches[year]) {
      winningMatches[year] = {};
    }
    //if the year doesnt exist in the array we create a new
    //object with that year name

    if (winningMatches[year][winner]) {
      winningMatches[year][winner]++;
      //if the winning team is already present in the object we increment its value
    } else if (winner === "") {
      /* if the match is undecided so it has empty value in the winner section
      in that case make a key with the value no result. */
      if (!winningMatches[year]["No result"]) {
        winningMatches[year]["No result"] = 1;
        //we create a key with "No result" and assign the value of 1 to it.
      } else {
        winningMatches[year]["No result"]++;
        //if there already exists a key with no-result we increment its value.
      }
    } else {
      winningMatches[year][winner] = 1;
      //if the winning team is not present in the object we
      //create a key with the winning team name with the value 1.
    }
  }
  return winningMatches;
  //return the object with result
}

fs.createReadStream("../data/matches.csv")
  .pipe(csv())
  //reading the .csv file
  .on("data", (data) => allMatches.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    try {
      const count = matchesWonPerTeamPerYear(allMatches);
      //calling the function with the allMatches array and storing in Count variable
      const output = "../public/output/matchesWonPerTeamPerYear.json";
      //the output is the location where the .json file needs to be dumped
      fs.writeFile(output, JSON.stringify(count, null, 2), (err) => {
        /*Passing the output variable to the writeFile function and 
        with the JSON.stringify with the matchCount variable which 
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
        if (err) throw err;
      });
    } catch (error) {
      console.log(error);
      //log the error if an error happens
    }
  });
