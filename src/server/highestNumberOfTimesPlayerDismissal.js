const csv = require("csv-parser");
const fs = require("fs");

const deliveries = [];

function playerDismissal(deliveries) {
  const result = {};
  //object to store the all the dismissals by a bowler

  for (let index = 0; index < deliveries.length; index++) {
    const currentBowler = deliveries[index].bowler;
    const dismissedPlayer = deliveries[index].player_dismissed;
    //iterating throught the deliveries array and assigning the
    //bowler and the player_dismissed to variables

    if (dismissedPlayer !== "") {
      //if the dismissedPlayer is not empty then someone has been dismissed on that ball

      if (!result[currentBowler]) {
        result[currentBowler] = {};
      }
      //if the bowler doesnt exist in the object we create the bowler with an empty object

      if (!result[currentBowler][dismissedPlayer]) {
        result[currentBowler][dismissedPlayer] = 1;
      } else {
        result[currentBowler][dismissedPlayer]++;
      }
    }
  }
  const finalResult = {};
  let finalBowler = "";
  let finalBatsman = "";
  let dismissalCount = 0;

  for (const bowler in result) {
    for (const dismissal in result[bowler]) {
      if (result[bowler][dismissal] > dismissalCount) {
        dismissalCount = result[bowler][dismissal];
        finalBowler = bowler;
        finalBatsman = dismissal;
      }
    }
  }
  finalResult.finalBowler = finalBowler;
  finalResult.finalBatsman = finalBatsman;
  finalResult.dismissalCount = dismissalCount;

  return finalResult;
}

fs.createReadStream("../data/deliveries.csv")
  .pipe(csv())
  //reading the .csv file
  .on("data", (data) => deliveries.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    console.log(playerDismissal(deliveries));
    try {
      const result = playerDismissal(deliveries);
      //calling the function with the allMatches array and storing in Count variabl
      const output =
        "../public/output/highestNumberOfTimesPlayerDismissal.json";
      //the output is the location where the .json file needs to be dumped
      fs.writeFile(output, JSON.stringify(result, null, 2), (err) => {
        /*Passing the output variable to the writeFile function and
        with the JSON.stringify with the result variable which
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
        if (err) throw err;
      });
    } catch (error) {
      console.log(error);
      //log the error if an error happens
    }
  });
