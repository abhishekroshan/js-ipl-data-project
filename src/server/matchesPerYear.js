const csv = require("csv-parser");
const fs = require("fs");
//importing the fs and csv-parser

const allMatches = [];
//creating an array to store all the matches from the csv file

function matchesPerYear(array) {
  const matchesPerYear = {};
  //the resultant object

  try {
    //try-catch for error related to the name or anything else
    for (let index = 0; index < array.length; index++) {
      const year = array[index].season;
      //storing the season of the current index in the "year" variable

      if (matchesPerYear[year]) {
        matchesPerYear[year]++;
        //if already has a value in the object we increment its count
      } else {
        matchesPerYear[year] = 1;
        //else we create a new value with the current year and assign its value as 1.
      }
    }
  } catch (error) {
    console.log(error);
    //if we encounter an error we come to the catch block and log the error
  }

  return matchesPerYear;
  //return the object after the function have executed properly
}

fs.createReadStream("../data/matches.csv")
  //reading the .csv file
  .pipe(csv())
  .on("data", (data) => allMatches.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    try {
      const matchCount = matchesPerYear(allMatches);
      //calling the function with the allMatches array and storing in matchCount variable

      const output = "../public/output/matchesPerYear.json";
      //the output is the location where the .json file needs to be dumped

      fs.writeFile(output, JSON.stringify(matchCount, null, 2), (err) => {
        /*Passing the output variable to the writeFile function and 
        with the JSON.stringify with the matchCount variable which 
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
        if (err) throw err;
      });
    } catch (error) {
      console.log(error);
      //log the error if an error happens
    }
  });
