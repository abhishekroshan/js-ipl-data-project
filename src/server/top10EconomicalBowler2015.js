const csv = require("csv-parser");
const fs = require("fs");
//importing the fs and csv-parser

const allMatches = [];
const deliveries = [];
//creating an array to store all the matches from the csv file

function topEconomicalBowler(allmatches, deliveries) {
  const result = {};
  //to store the economy of all the bowlers

  for (let index = 0; index < deliveries.length; index++) {
    const delivery = deliveries[index];
    const matchId = delivery.match_id;
    //assigning the match_id in matchid variable

    for (let newIndex = 0; newIndex < allMatches.length; newIndex++) {
      if (
        allMatches[newIndex].id === matchId &&
        allMatches[newIndex].season === "2015"
      ) {
        //if the matchid of allmatches array is same as deliveries array
        //and year is 2015 then we assign bowler to the bowler variable
        //and totalruns and extraruns in other variable
        const bowler = delivery.bowler;
        const totalRuns = Number(delivery.total_runs);
        const extraRuns = Number(delivery.extra_runs);

        if (!result[bowler]) {
          result[bowler] = {
            totalRuns: 0,
            totalBalls: 0,
          };
        }
        //the the bowler doesnt exists in the object we create the
        //bowler with the initial value of totalruns and totalballs to 0.
        result[bowler].totalRuns += totalRuns + extraRuns;
        result[bowler].totalBalls++;
        //we increment the balls count by 1 and runs count by the totalruns
      }
    }
    /*Finding economy */
    for (const bowler in result) {
      const totalBalls = result[bowler].totalBalls;
      const totalRuns = result[bowler].totalRuns;
      //iterating through the result object and storing totalBalls and
      //totalRuns in variable

      const economyRates = ((totalRuns / totalBalls) * 6).toFixed(2);
      //economy calculation formulae

      result[bowler].economyRates = economyRates;
      //creating a newfield in the result object with the vlue of economyrates
    }
  }
  const sortedBowlers = Object.keys(result).sort((a, b) => {
    return result[a].economyRates - result[b].economyRates;
  });
  //use object.keys to get the keys and sorted the value based on the
  //economy of those keys

  const topBowlers = [];
  //top bowlers array

  for (let index = 0; index < 10; index++) {
    //since me need only top 10 bowlers so we run loop till 9 only
    const bowler = sortedBowlers[index];
    //current bowler
    topBowlers.push({ [bowler]: result[bowler] });
    //pushing the bowler and the stats related to that bowler into the array as a object.
  }
  return topBowlers;
}

fs.createReadStream("../data/matches.csv")
  //reading the .csv file
  .pipe(csv())
  .on("data", (data) => allMatches.push(data))
  //pushing the values in the allMatches Array
  .on("end", () => {
    fs.createReadStream("../data/deliveries.csv")
      //reading the .csv file
      .pipe(csv())
      .on("data", (data) => deliveries.push(data))
      //pushing the values in the deliveries Array
      .on("end", () => {
        try {
          const result = topEconomicalBowler(allMatches, deliveries);
          //calling the function topEconomicalBowlers with allMatch and deliveries array.

          const output = "../public/output/topEconomicalBowlers.json";
          //the output is the location where the .json file needs to be dumped

          fs.writeFile(output, JSON.stringify(result, null, 2), (err) => {
            /*Passing the output variable to the writeFile function and
        with the JSON.stringify with the result variable which
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
            if (err) throw err;
          });
        } catch (error) {
          console.log(error);
          //log the error if an error happens
        }
      });
  });
