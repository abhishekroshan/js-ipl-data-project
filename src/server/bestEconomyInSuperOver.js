const csv = require("csv-parser");
const fs = require("fs");

const deliveries = [];

function bestEconomyInSuperOver(deliveries) {
  const result = {};

  for (let index = 0; index < deliveries.length; index++) {
    const delivery = deliveries[index];

    if (delivery.is_super_over === "1") {
      const bowler = delivery.bowler;

      const totalRuns = Number(delivery.total_runs);
      if (!result[bowler]) {
        result[bowler] = {
          runs: 0,
          balls: 0,
        };
      }
      result[bowler].runs += totalRuns;
      result[bowler].balls++;
    }
  }
  const allEconomy = {};

  for (const bowler in result) {
    const runs = result[bowler].runs;
    const balls = result[bowler].balls;
    const overs = balls / 6;

    const economy = runs / overs;

    allEconomy[bowler] = economy;
  }

  let bestBowler = "";
  let bestEconomy = Number.MAX_SAFE_INTEGER;

  for (const bowler in allEconomy) {
    if (allEconomy[bowler] < bestEconomy) {
      bestBowler = bowler;
      bestEconomy = allEconomy[bowler];
    }
  }
  const finalResult = {};

  finalResult.bestBowler = bestBowler;
  finalResult.bestEconomy = bestEconomy;

  return finalResult;
}

fs.createReadStream("../data/deliveries.csv")
  .pipe(csv())
  //reading the .csv file
  .on("data", (data) => deliveries.push(data))
  //pushing the values in the deliveries Array
  .on("end", () => {
    console.log(bestEconomyInSuperOver(deliveries));
    try {
      const result = bestEconomyInSuperOver(deliveries);
      //calling the function with the deliveries array and storing in result variabl
      const output = "../public/output/bestEconomyInSuperOver.json";
      //the output is the location where the .json file needs to be dumped
      fs.writeFile(output, JSON.stringify(result, null, 2), (err) => {
        /*Passing the output variable to the writeFile function and
        with the JSON.stringify with the result variable which
        contains the desired value.
        with a callback with error if the error happens it will execute the catch block */
        if (err) throw err;
      });
    } catch (error) {
      console.log(error);
      //log the error if an error happens
    }
  });
